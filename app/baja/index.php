<!DOCTYPE html>
<html lang="es">

<head>
        <title>Sistema de Registro</title>
        <!--Bootstrap css-->
        <link rel="stylesheet" href="../../plugins/bootstrap-4.3.1-dist/css/bootstrap.min.css">
</head>

<body>
        <div class="container-fluid">
                <header>
                  <!-- navbar -->
                <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                        <a class="navbar-brand" href="../../">Sistema de Registro</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" area-expanded="false" aria-label="toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item">
                                   <a class="nav-link" href="../listado">Listado</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="../alta/index.php">Nuevo</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="../modificacion">Editar</a>
                                 </li>
                                <li class="nav-item">
                                    <a class="nav-link active" href="../baja">Borrar</a>
                                </li>
                        </ul>
                           <span class="navbar-text">
                            Hola Cosme Fulanito! &nbsp;
                        </span>
                         <button type="button" class="btn btn-sm btm-outlime-light">Cerrar Sesion</button>
                 </div>
        </nav><!-- ./navbar -->
        </header>
                <hr>
                <h2>Eliminar un registro</h2>

                <form action="buscar.php" method="POST">
			      DNI:
                  <input type="text" name="dni" required>
		         <br><br>
		 		SEXO:
                <input type="radio" name="sexo" value="F" checked> Femenino
			    <input type="radio" name="sexo" value="M"> Masculino
		        <br><br>
			<input type="submit" value="BORRAR">
        </div>
</form>

</div>
        <!-- jQuery JS -->
        <script src="../../plugins/jQuery/jquery-3.5.1.min.js"></script>
        <!-- Bootstrap JS -->
        <script src="../../plugins/bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>
</body>

</html>