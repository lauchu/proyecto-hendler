<DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>EJERCICIOS CON PHP</title>
</head>
<body>
    <h1>HOLA MUNDO</h1>
    <?php
    $arr = array ([0, "casa", "verde", 3, 5]);
   var_dump  ($arr);
    echo '<br><br>';
    ?>
    <?php
    $datos[0] = 0;
    $datos[1] = 'casa';
    $datos[2] = 'verde';
    $datos[3] = 3;
    $datos[4] = 5;
    echo "cantidad de elementos:" .count($datos);
    echo '<br> <br>';
    ?>
    <?php
    $dato = array ('nombre'=>'fulanito <br>', 'apellido'=>'Cosme <br>', 'email'=>'cosmefulanito@gmail.com');
     print_r ($dato);
    echo '<br><br>';
    ?>
    <?php
    $dato = array ('dni'=>"22333444<br>", 'edad'=>'33');
    print_r ($dato);
    echo '<br><br>';
    ?>
    <?php
    echo 'DNI:22333444<br>';
    echo 'Apellido y Nombre:Cosme Fulanito<br>';
    echo 'Email:cosmefulanito@gmail.com';
    echo '<br><br>';
    ?>
    <?php
    $numero = 1;
    while ($numero <= 10)
    {
        echo $numero;
        $numero =$numero +1;
    }

    ?>    

<!-- jQuery JS -->
<script src="./plugins/jQuery/jquery-3.5.1.min.js"></script>

<!-- Bootstrap JS -->
<script src="./plugins/bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>
</body>
</html>