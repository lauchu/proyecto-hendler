<!doctype html>
<html lang="es">

<head>
    <title>Sistema de Registro</title>
    <!--Required meta tags -->
    <meta chareset="utf-8">
    <meta name="viewport" content="whidth=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- Bootstrap css -->
    <link rel="stylesheet" href="./plugins/bootstrap-4.3.1-dist/css/bootstrap.min.css">
</head>
<body>
    <div class="container-fluid">
    
    <header>
        
      <!-- navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="#">Sistema de Registro</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
        aria-controls="navbarNav" area-expanded="false" aria-label="toggle navigation">
                <span class="navbar-toggler-icon"></span>
</button>
        <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav mr-auto">                         
            <li class="nav-item">
                <a class="nav-link" href="./app/listado">Listado</a>
</li>
        <li class="nav-item">
          <a class="nav-link" href="./app/alta">Nuevo</a>
            </li>
             <li class="nav-item">
        <a class="nav-link" href="./app/modificacion">Editar</a>
</li>
            <li class="nav-item">
        <a class="nav-link" href="./app/baja">Borrar</a>  
</li>
</ul>
        <span class="navbar-text">
            Hola Cosme Fulanito! &nbsp;
</span>
        <button type="button" class="btn btn-sm btm-outlime-light">Cerrar Sesion</button>
</div>    
        </nav><!-- ./navbar -->
        </header>

        <div class="container">
            <hr>
            <h2>BIENVENIDOS</h2>
            <hr>
        </div><!-- ./container -->

    </div><!-- ./container-fluid -->

    <!-- jQuery JS -->
    <script src="./plugins/jQuery/jquery-3.5.1.min.js"></script>

    <!-- Bootstrap JS -->
    <script src="./plugins/bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>
    
</body>

</html>
                   
