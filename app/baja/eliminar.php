<?php
include '../funciones.php';
$id	= $_POST['id'];

/****** Eliminar Registro por ID ******/
$link 	= conexion();
$sql    = "DELETE FROM personas WHERE id = '$id'";

mysqli_query($link, $sql) or die(mysqli_error($link));
/* Las sentencias del tipo DELETE no devuelven un listado, por lo tanto
 * no es necesario guardar los resultados en una variable ($res)
 * */

$filas_affectadas = mysqli_affected_rows($link); //Cantidad de filas afectadas en la última consulta ejecutada

/* Debido a que las sentencias del tipo DELETE no devuelven un listado, 
 * hacemos la evaluación utilizando la cantidad de filas afectadas
 * Si $filas_affectadas > 0 entonces se ejecutó la consulta (se insertó el registro)
 * */

if ($filas_affectadas > 0) {
    echo "Regitro eliminado!",
    "<a href ='./'>VOLVER</a>"; //Confirmación
} else {
    echo "Ha ocurrido un error, por favor, intentelo nuevamente <br><br>",
    "<a href ='./'>VOLVER</a>"; //Mensaje de error
}

/****** Cerrar conexión a la Base de Datos  ******/
mysqli_close ($link);