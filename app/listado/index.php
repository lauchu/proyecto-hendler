<?php
include '../funciones.php';
$i	= 0;
$link   = conexion();
$sql	= "SELECT * FROM personas";
$res	= mysqli_query($link, $sql) or die(mysqli_error($link));
mysqli_close($link);

?>

<!DOCTYPE html>
<html lang="es">

<head>
	<title>Listado de Personas</title>

	<!-- Meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

	<!-- DataTables CSS -->
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
</head>

<body>
	<div class="container">
		<header>
			<!-- navbar -->
			<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
				<a class="navbar-brand" href="../../">Sistema de Registro</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" area-expanded="false" aria-label="toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNav">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item">
							<a class="nav-link" href="../listado">Listado</a>
						</li>
						<li class="nav-item">
							<a class="nav-link active" href="../alta/index.php">Nuevo</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="../modificacion">Editar</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="../baja">Borrar</a>
						</li>
					</ul>
			</nav><!-- ./navbar -->
			<hr>
			<h2>LISTADO DE PERSONAS</h2>
			<hr>
		</header>
		<table id="personas" class="display">
			<thead>
				<tr>
					<th>#</th>
					<th>DNI</th>
					<th>NOMBRE Y APELLIDO</th>
					<th>EMAIL</th>
					<th>SEXO</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th>#</th>
					<th>DNI</th>
					<th>NOMBRE Y APELLIDO</th>
					<th>EMAIL</th>
					<th>SEXO</th>
				</tr>
			</tfoot>
			<tbody>
				<?php
				while ($row = mysqli_fetch_assoc($res)) {
					$i++;
					echo
						"<tr>
				 <td> $i </td>
				 <td>{$row['dni']}</td>
		         <td>{$row['apyn']}</td>
		         <td>{$row['email']}</td>
		         <td>{$row['sexo']}</td>
				</tr>";
				}

				?>
			<tbody>
		</table>
	</div>

	<!-- jQuery - Popper.js - Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

	<!-- DataTables JS -->
	<script src="//cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>

	<!-- Inicializar DataTables -->
	<script>
		$(document).ready(function() {
			$('#personas').DataTable();
		});
	</script>

</body>

</html>