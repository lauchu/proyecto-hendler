<?php
include '../funciones.php';

 $link = conexion();

 $sql_hob = "SELECT * FROM hobbies";
 $res_hob = mysqli_query($link, $sql_hob);
    

$sql_ocup = "SELECT * FROM ocupaciones";
$res_ocup = mysqli_query($link, $sql_ocup);

 
mysqli_close ($link);

?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Formulario</title>
    <!--Bootstrap css-->
    <link rel="stylesheet" href="../../plugins/bootstrap-4.3.1-dist/css/bootstrap.min.css">
</head>

<body>
    <div class="container-fluid">
        <header>
            <!-- navbar -->
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="../../">Sistema de Registro</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" area-expanded="false" aria-label="toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="../listado">Listado</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="../alta/index.php">Nuevo</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../modificacion">Editar</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../baja">Borrar</a>
                        </li>
                    </ul>
                    <span class="navbar-text">
                        Hola Cosme Fulanito! &nbsp;
                    </span>
                    <button type="button" class="btn btn-sm btm-outlime-light">Cerrar Sesion</button>
                </div>
            </nav><!-- ./navbar -->
        </header>
        <hr>
        <h1>Formulario de Alta</h1>
        <hr>
        <section>
            <form action="guardar.php" method="POST">
                <div class="row">
                    <div class=col-md-6>
                        <label for="dni">DNI:</label>
                        <input type="text" name="dni" placeholder="ej: 25431286" class="form-control">
                        <br>
                    </div>
                    <div class="col-md-6">
                        <label for="apyn">Nombre y Apellido:</label>
                        <input type="text" name="apyn" id="apyn" class="form-control">
                        <br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">

                        <label for="email"> Email:</label>
                        <input type="email" name="email"  class="form-control">
                        <br>
                    </div>

                    <div class="col-md-6">
                        <div class="form-check">
                            <label for="sexo">Sexo:</label>
                            <br>
                            <input type="radio" name="sexo" value="F" checked><span> Femenino </span>
                            <input type="radio" name="sexo" value="M"><span> Masculino </span>
                            <br>
                        </div>
                    </div>
                </div><!-- ./row -->

                <div class="row">
                    <div class="col-md-6 form-group"> 
                        <label> Hobbies: </label>
                        <div>
                        <?php
                        while ($row_hob = mysqli_fetch_assoc($res_hob)) {
                         echo
                            "<div class='form-group form-check-inline'>
                            <input class='form-check-input' type='checkbox' name='hobbies[]' value='{$row_hob['id']}'>
                            <span class='form-check-label'>{$row_hob['detalle']} </span>
                             </div>";
                        }
                        ?>
                        </div>
                       </div><!--./col -->
                
                <div class="col-md-6 form-group">
                    <label for="ocupacion">ocupación:</label>
                    <select name="ocupaciones"  class="form-control">
                        <option value="0">--seleccione--</option>
                        <?php
                             while ($row_ocup = mysqli_fetch_assoc($res_ocup)) {
                                  echo "<option value='{$row_ocup['id']}'>{$row_ocup['detalle']}</option>";
                       }
                    ?>
                    </select>
                </div>
            </div>
             <div class="row">
                    <div class="col-md-12">
                        <label for="sugerencias">sugerencias</label>
                        <textarea rows="3" name="sugerencias" class="form-control"placeholder ="Ingrese un comentario"></textarea>
                    </div>
                </div><!-- ./row -->
    
                <div class="row justify-content-center">
                    <div class="col-md-2">
                        <input type="submit" class="btn btn-primary btn-lg active" value="ENVIAR">
                    </div>
                    <div class="col-md-2">
                        <input type="button" class="btn btn-secondary btn-lg" value="CANCELAR">
                    </div>
                </div>
            </form>
        </section>
    </div><!-- ./container-fluid -->

    <!-- jQuery JS -->
    <script src="../../plugins/jQuery/jquery-3.5.1.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="../../plugins/bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>
</body>

</html>